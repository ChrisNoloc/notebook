# Youtube Notebook

## Application specifics

A subject, a folder : save and organize your favorite Youtube videos (training, passion, etc.).

###### Current Features

- Google authentication
- CRUD folder
- Add subfolder or video (YouTube video link) to your folder
- CRD video
- Add multiple videos (YouTube playlist link)
- *Preload every videos of a folder into a playlist you can read offline (**Work in progress**)*

###### Technologies

- Angular 7.2 : components, services, router, guard, pipes, directives, models
- Librairies : RxJS 6.5, angularfire2, ngx-youtube-player
- Cloud Firestore
- Bootstrap 4
- SCSS

## About me

Passionated about web development, this project is a way for me to exercice and have fun. I'm looking to keep learning, so please make any helpful suggestions.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
