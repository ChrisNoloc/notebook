import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectComponent } from './subject/subject.component';
import { PublicHomeComponent } from './home/public-home/public-home.component';
import { OwnerGuard } from '@shared/services/authentication/owner.guard';
import { PlaylistComponent } from './playlist/playlist.component';

const routes: Routes = [
  {
    path: '',
    component: PublicHomeComponent
  },
  {
    path: 'subject/:idSubject',
    component: SubjectComponent,
    canActivate: [OwnerGuard]
  },
  {
    path: 'playlist',
    component: PlaylistComponent
  },
  {
    path: '**',
    component: PublicHomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
