import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {

  transform(date, format) {
    let transformedDate = date;

    if (format != null) {
      transformedDate = moment(date).locale('fr').format(format);
    } else {
      transformedDate = moment(date).locale('fr').fromNow();
    }

    return transformedDate;
  }

}
