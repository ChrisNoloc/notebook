import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'spacing'
})
export class SpacingPipe implements PipeTransform {

  transform(spacing: number): any {
    let spaceString = '';

    for (let i = 0; i < spacing; i++) {
      spaceString += '\u00A0\u00A0\u00A0\u00A0\u00A0';
    }

    return spaceString;
  }

}
