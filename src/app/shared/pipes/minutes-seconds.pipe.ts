import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutesSeconds'
})
export class MinutesSecondsPipe implements PipeTransform {

  // Converts a number of seconds into a 'minutes:seconds' string
  transform(value: number): string {
    value = Math.round(value);

    let minutes = 0;
    let seconds = '0';

    try {
      minutes = Math.floor(value / 60);
      seconds = String(value - minutes * 60);

      seconds = Number(seconds) < 10 ? '0' + seconds : seconds;
    } catch {

    }

    return `${minutes}:${seconds}`;
  }

}
