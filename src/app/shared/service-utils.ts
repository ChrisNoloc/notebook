import {
  DocumentChangeAction,
  DocumentSnapshot,
  Action
} from 'angularfire2/firestore';
import { Theme } from './models/theme';

export class ServiceUtils {
  getObjectFromFirebaseCollection<T>(action: DocumentChangeAction<{}>): T {
    const data = action.payload.doc.data() as T;
    const id = action.payload.doc.id;

    return { id, ...data };
  }

  getObjectFromFirebaseDocument<T>(doc: Action<DocumentSnapshot<{}>>) {
    const data = doc.payload.data() as T;
    const id = doc.payload.id;

    return { id, ...data };
  }
}
