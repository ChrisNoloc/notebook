import { Directive, Output, EventEmitter, HostListener } from '@angular/core';
import { Subject, Observable, interval } from 'rxjs';
import { filter, tap, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[holdable]'
})
export class HoldableDirective {
  @Output() holdTime: EventEmitter<number> = new EventEmitter();

  state: Subject<string> = new Subject();
  cancel: Observable<string>;

  constructor() {
    this.cancel = this.state.pipe(
      filter(v => v === `cancel`),
      tap(v => {
        console.log(`%c Stopped hold`, `color : #ec6969; font-weight: bold;`);
        this.holdTime.emit(0);
      })
    );
  }

  // Trigger cancel when 'mouseup' or 'mouseleave'
  @HostListener(`mouseup`, ['$event'])
  @HostListener('mouseleave', ['$event'])
  onExit() {
    this.state.next('cancel');
  }

  //
  @HostListener('mousedown', ['$event'])
  onHold() {
    console.log('%c Started hold', 'color: #5fba7d; font-weight: bold;');

    this.state.next('start');

    const n = 100;

    interval(n)
      .pipe(
        takeUntil(this.cancel),
        tap(v => {
          if (v < 20) {
            this.holdTime.emit(v * n);
          } else {
            this.state.next('cancel');
          }
        })
      )
      .subscribe();
  }
}
