import { PlaylistItemYTApi } from "./playlist-item-yt-api";

export class PlaylistYTApi {
    kind: string;
    etag: string;
    nextPageToke: string;
    pageInfo: {
        totalResults: number;
        resultsPerPage: number;
    };
    items: PlaylistItemYTApi[];
}
