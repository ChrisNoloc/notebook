export class FolderTree {
    name: String;
    id: String;
    videos: Number;
    subfolders: FolderTree[];
    spacing: number;
}
