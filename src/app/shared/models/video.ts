import { Timestamp } from 'rxjs/internal/operators/timestamp';

export interface Video {
  id?: string;
  channelTitle: string;
  channelId: string;
  titre: string;
  idSujet: string;
  dateCreation: any;
  tags: string[];
  youtubeId: string;
  thumbnails: {
    default: {
      url: string;
      width: Number;
      height: Number;
    };
    medium: {
      url: string;
      width: Number;
      height: Number;
    };
    high: {
      url: string;
      width: Number;
      height: Number;
    };
    standard: {
      url: string;
      width: Number;
      height: Number;
    };
    maxres: {
      url: string;
      width: Number;
      height: Number;
    };
  };
  publishedAt: Date;
  statistics: {
    viewCount: Number;
    likeCount: Number;
    dislikeCount: Number;
    favoriteCount: Number;
    commentCount: Number;
  };
  description: string;
}
