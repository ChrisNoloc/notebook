export class PlaylistItemYTApi {
  kind: string;
  etag: string;
  id: string;
  contentDetails: {
    videoId: string;
    videoPublishedAt: Date;
  };
}
