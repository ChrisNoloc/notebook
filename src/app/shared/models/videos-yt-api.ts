export interface VideosYTApi {
  kind: string;
  string: string;
  pageInfo: {
    totalResults: Number;
    resultsPerPage: Number;
  };
  items: [
    {
      kind: string;
      string: string;
      id: string;
      snippet: {
        publishedAt: Date;
        channelId: string;
        title: string;
        description: string;
        thumbnails: {
          default: {
            url: string;
            width: Number;
            height: Number;
          };
          medium: {
            url: string;
            width: Number;
            height: Number;
          };
          high: {
            url: string;
            width: Number;
            height: Number;
          };
          standard: {
            url: string;
            width: Number;
            height: Number;
          };
          maxres: {
            url: string;
            width: Number;
            height: Number;
          };
        };
        channelTitle: string;
        tags: [string];
        categoryId: string;
        liveBroadcastContent: string;
        defaultLanguage: string;
        localized: {
          title: string;
          description: string;
        };
        defaultAudioLanguage: string;
      };

      statistics: {
        viewCount: Number;
        likeCount: Number;
        dislikeCount: Number;
        favoriteCount: Number;
        commentCount: Number;
      };
      player: {
        embedHtml: string;
        embedHeight: Number;
        embedWidth: Number;
      };
      topicDetails: {
        topicIds: [string];
        relevantTopicIds: [string];
        topicCategories: [string];
      };
      recordingDetails: {
        recordingDate: Date;
      };
      fileDetails: {
        fileName: string;
        fileSize: Number;
        fileType: string;
        container: string;
        videoStreams: [
          {
            widthPixels: Number;
            heightPixels: Number;
            frameRateFps: Number;
            aspectRatio: Number;
            codec: string;
            bitrateBps: Number;
            rotation: string;
            vendor: string;
          }
        ];
        audioStreams: [
          {
            channelCount: Number;
            codec: string;
            bitrateBps: Number;
            vendor: string;
          }
        ];
        durationMs: Number;
        bitrateBps: Number;
        creationTime: string;
      };
      processingDetails: {
        processingStatus: string;
        processingProgress: {
          partsTotal: Number;
          partsProcessed: Number;
          timeLeftMs: Number;
        };
        processingFailureReason: string;
        fileDetailsAvailability: string;
        processingIssuesAvailability: string;
        tagSuggestionsAvailability: string;
        editorSuggestionsAvailability: string;
        thumbnailsAvailability: string;
      };
      suggestions: {
        processingErrors: [string];
        processingWarnings: [string];
        processingHints: [string];
        tagSuggestions: [
          {
            tag: string;
            categoryRestricts: [string];
          }
        ];
        editorSuggestions: [string];
      };
      liveStreamingDetails: {
        actualStartTime: Date;
        actualEndTime: Date;
        scheduledStartTime: Date;
        scheduledEndTime: Date;
        concurrentViewers: Number;
        activeLiveChatId: string;
      };
    }
  ];
}
