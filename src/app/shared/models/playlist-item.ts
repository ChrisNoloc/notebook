import { Video } from './video';

export class PlaylistItem {
  video: Video;
  index: number;
}
