export interface Theme {
    id?: string;
    owner: string;
    libelle: string;
    themeParent: string;
    dateCreation: Date;
}
