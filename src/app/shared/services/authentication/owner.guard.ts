import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { SujetService } from '../subject/sujet.service';
import { take, map, tap, switchMap, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OwnerGuard implements CanActivate {
  constructor(
    private as: AuthService,
    private ss: SujetService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const subjectId = next.paramMap.get('idSubject');

    return this.as.user$.pipe(
      take(1),
      switchMap(user => {
        // If the visitor is authenticated
        if (!!user) {
          // Get the folder from the ID given in the route
          return this.ss.getThemeById(subjectId).pipe(
            // Check if the connected user is the owner of this folder
            map(subject => subject.owner === user.uid)
          );
        } else {
          console.log(`✋ Access denied : visitor isn't authenticated`);
          this.router.navigate(['/']);

          return of(false);
        }
      }),
      tap(isOwner => {
        if (!isOwner) {
          console.log(
            `✋ Access denied : the connected user doesn't own the folder`
          );
          this.router.navigate(['/']);
        }
      })
    );
  }
}
