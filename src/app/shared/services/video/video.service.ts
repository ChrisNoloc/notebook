import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of, ObservableInput } from 'rxjs';
import { map, catchError, shareReplay } from 'rxjs/operators';
import { AngularFirestore } from 'angularfire2/firestore';
import { Video } from '../../models/video';
import { Theme } from '../../models/theme';
import { VideosYTApi } from '../../models/videos-yt-api';
import { PlaylistYTApi } from '../../models/playlist-yt-api';
import { ServiceUtils } from '@shared/service-utils';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class VideoService {
  constructor(private afs: AngularFirestore, private _http: HttpClient) {}

  private API_KEY = 'AIzaSyBQBadKsBdWspZPrC5ErOksLQP7NSdazt8';
  private YOUTUBE_API_ENDPOINT = 'https://www.googleapis.com/youtube/v3';
  private utils = new ServiceUtils();

  videos$ = this.afs
    .collection('videos', ref => ref.orderBy('dateCreation'))
    .snapshotChanges()
    .pipe(
      map(actions =>
        actions.map(action =>
          this.utils.getObjectFromFirebaseCollection<Video>(action)
        )
      ),
      shareReplay()
    );

  getAllVideosByIdSujet(idSujet: string) {
    return this.afs
      .collection('videos', refs =>
        refs.where('idSujet', '==', idSujet).orderBy('dateCreation')
      )
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(action =>
            this.utils.getObjectFromFirebaseCollection<Video>(action)
          )
        )
      );
  }

  // Process submitted link : what type of link was submitted : video, playlist, link
  processSubmittedLink(link: any) {
    // Get Ids of video and playlist from Youtube API
    if (this.isLink(link.input)) {
      const ids = this.extractYoutubeIdsFromUrl(link.input);

      if (!!ids.playlistId) {
        // If url was a playlist URL, get youtubeIds from playlist items and insert all the videos
        this.getYoutubePlaylistVideos(ids.playlistId)
          .then(playlist => {
            playlist.items.forEach(v => {
              this.insertVideo(v.contentDetails.videoId, link.idSujet);
            });
          })
          .catch(this.handleError);
      } else if (!!ids.videoId) {
        // If url was a simple video, insert video
        this.insertVideo(ids.videoId, link.idSujet);
      } else {
        console.log(`Invalid Youtube Video : ${link}`);
      }
    }
  }

  deleteVideo(video: Video) {
    this.afs
      .collection('videos')
      .doc(video.id)
      .delete();
  }

  insertVideo(youtubeId: string, subjectId: string): string {
    let id: string = null;

    if (!!youtubeId && !!subjectId) {
      if (!this.isDuplicate(youtubeId, subjectId)) {
        const video = this.getVideoFromYoutubeId(youtubeId, subjectId);

        if (!!video) {
          this.afs
            .collection('videos')
            .add(video)
            .then(docRef => (id = docRef.id))
            .catch(this.handleError);
        }
      }
    }

    return id;
  }

  private isLink(link: string): boolean {
    const regex = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i'
    ); // fragment locator

    const result = regex.test(link);

    return result;
  }

  // Verify if there is another video with the same Youtube Id and Subject Id
  private isDuplicate(videoId: string, subjectId: string): boolean {
    let result = false;

    if (!!videoId && !!subjectId) {
      this.getAllVideosByIdSujet(subjectId)
        .toPromise()
        .then(videos => {
          result = videos.some(v => v.youtubeId === videoId);
        })
        .catch(this.handleError);
    }

    return result;
  }

  // Insert Video into database
  private getVideoFromYoutubeId(youtubeId: string, subjectId: string): Video {
    let video: Video = null;

    this.getYoutubeVideo(youtubeId)
      .then(ytVideo => {
        video = {
          channelTitle: ytVideo.items[0].snippet.channelTitle,
          channelId: ytVideo.items[0].snippet.channelId,
          titre: ytVideo.items[0].snippet.title,
          youtubeId: ytVideo.items[0].id,
          thumbnails: ytVideo.items[0].snippet.thumbnails,
          idSujet: subjectId,
          dateCreation: new Date(),
          tags: ytVideo.items[0].snippet.tags,
          publishedAt: ytVideo.items[0].snippet.publishedAt,
          statistics: ytVideo.items[0].statistics,
          description: ytVideo.items[0].snippet.description
        };
      })
      .catch(this.handleError);

    return video;
  }

  private extractYoutubeIdsFromUrl(url: string): any {
    let videoId: string = null;
    let playlistId: string = null;

    const startVideoId = url.indexOf('v=') + 2;
    if (startVideoId > -1) {
      if (url.indexOf('list') > -1) {
        // Url of a Youtube playlist
        const endVideoId = url.indexOf('&', startVideoId);

        videoId = url.substring(startVideoId, endVideoId);

        const startPlaylistId = url.indexOf('=', endVideoId) + 1;
        const endPlaylistId = url.indexOf('&', startPlaylistId);

        if (endPlaylistId > -1) {
          // Url of type : https://www.youtube.com/watch?v=...&list=...&index=...
          playlistId = url.substring(startPlaylistId, endPlaylistId);
        } else {
          // Url of type : https://www.youtube.com/watch?v=...&list=...
          playlistId = url.substring(startPlaylistId);
        }
      } else if (url.indexOf('&', startVideoId) > -1) {
        // Url of type : https://www.youtube.com/watch?v=...&t=...
        videoId = url.substring(startVideoId, url.indexOf('&', startVideoId));
      } else {
        // Url of simple Youtube video : https://www.youtube.com/watch?v=...
        videoId = url.substring(startVideoId);
      }
    }

    return {
      videoId: videoId,
      playlistId: playlistId
    };
  }
  /*
    Http Get Requests : Youtube Data API
  */
  private getYoutubeVideo(id): Promise<VideosYTApi> {
    const url =
      this.YOUTUBE_API_ENDPOINT +
      '/videos?id=' +
      id +
      '&key=' +
      this.API_KEY +
      '&part=snippet,statistics';

    return this._http
      .get<VideosYTApi>(url)
      .pipe(catchError(this.handleError))
      .toPromise();
  }

  private getYoutubePlaylistVideos(id): Promise<PlaylistYTApi> {
    const url =
      this.YOUTUBE_API_ENDPOINT +
      '/playlistItems?playlistId=' +
      id +
      '&key=' +
      this.API_KEY +
      '&part=contentDetails&maxResults=50';

    return this._http
      .get<PlaylistYTApi>(url)
      .pipe(catchError(this.handleError))
      .toPromise();
  }

  private handleError(err) {
    console.error(err);

    return throwError(err);
  }
}

// Example of API url to search information about a Youtube Video :
// https://www.googleapis.com/youtube/v3/videos?id=kl-UMCHpEsw&key=AIzaSyBQBadKsBdWspZPrC5ErOksLQP7NSdazt8&part=snippet
