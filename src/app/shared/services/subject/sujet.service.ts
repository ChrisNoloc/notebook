import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, switchMap, shareReplay, find, tap, take } from 'rxjs/operators';
import { AngularFirestore } from 'angularfire2/firestore';
import { VideoService } from '../video/video.service';
import { Theme } from '../../models/theme';
import { FolderTree } from '../../models/folder-tree';
import { ServiceUtils } from '@shared/service-utils';

@Injectable()
export class SujetService {
  themes$: Observable<Theme[]>;
  folders: FolderTree[] = [];
  foldersList: FolderTree[];
  utils = new ServiceUtils();

  constructor(private afs: AngularFirestore, private vs: VideoService) {}

  folders$ = this.afs
    .collection('themes')
    .snapshotChanges()
    .pipe(
      map(actions =>
        actions.map(action =>
          this.utils.getObjectFromFirebaseCollection<Theme>(action)
        )
      ),
      shareReplay()
    );

  parentFolders$ = this.folders$.pipe(
    map(folders => folders.filter(f => f.themeParent == null))
  );

  getThemeById(idTheme: string): Observable<Theme> {
    return this.afs
      .collection('themes')
      .doc(idTheme)
      .snapshotChanges()
      .pipe(
        map(action => this.utils.getObjectFromFirebaseDocument<Theme>(action))
      );
  }

  getAllSujetsByIdTheme(idTheme) {
    return this.folders$.pipe(
      map(folders => folders.filter(f => f.themeParent === idTheme))
    );
  }

  insertTheme(theme) {
    return this.afs.collection('themes').add({
      owner: theme.userId,
      libelle: theme.titre,
      themeParent: theme.idTheme,
      dateCreation: new Date()
    });
  }

  updateTheme(theme) {
    this.afs
      .collection('themes')
      .doc(theme.id)
      .update(theme);
  }

  deleteTheme(theme) {
    // Delete all related videos
    this.vs.getAllVideosByIdSujet(theme.id).subscribe(videos => {
      videos.forEach(v => this.vs.deleteVideo(v));
    });

    // Delete all related subfolders
    this.getAllSujetsByIdTheme(theme.id).subscribe(subjects => {
      subjects.forEach(s => this.deleteTheme(s));
    });

    // Delete the subject
    this.afs
      .collection('themes')
      .doc(theme.id)
      .delete();
  }
}
