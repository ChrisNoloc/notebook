import { Injectable, ChangeDetectionStrategy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { PlaylistItem } from '../../models/playlist-item';
import { Video } from '@shared/models/video';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {
  constructor() {}

  private playlistItemsSource = new BehaviorSubject<PlaylistItem[]>([]);
  playlistItems$ = this.playlistItemsSource.asObservable();

  private playlistStateSource = new BehaviorSubject<string>('stopped');
  currentState$ = this.playlistStateSource.asObservable();

  private itemsReadyToLoadSource = new BehaviorSubject<PlaylistItem[]>([]);

  private itemLoadedSource = new BehaviorSubject<PlaylistItem[]>([]);
  itemLoaded$ = this.itemLoadedSource.asObservable();

  private itemPlayingSource = new Subject<PlaylistItem>();
  itemPlaying$ = this.itemPlayingSource.asObservable();

  private pausedItemSource = new Subject<boolean>();
  pausedItem$ = this.pausedItemSource.asObservable();

  private playerProgressSource = new BehaviorSubject<number>(0);
  playerProgress$ = this.playerProgressSource.asObservable();

  /**
   * Playlist globals
   */

  setPlaylistItems(items: PlaylistItem[]) {
    this.playlistItemsSource.next(items);
  }

  addVideoToPlaylist(video: Video) {
    const playlistItems = this.playlistItemsSource.getValue();
    const item = {
      video: video,
      index: playlistItems.length
    };

    this.setPlaylistItems(playlistItems);
  }

  changeState(state: string) {
    this.playlistStateSource.next(state);
  }

  itemReadyToLoad(item: PlaylistItem) {
    const itemsReadyToLoad = this.itemsReadyToLoadSource.getValue();
    const playlistItems = this.playlistItemsSource.getValue();

    itemsReadyToLoad.push(item);

    if (itemsReadyToLoad.length === playlistItems.length) {
      this.changeState('readyToLoad');
    }

    this.itemsReadyToLoadSource.next(itemsReadyToLoad);
  }

  /**
   * Playlist Loading
   */

  startLoading() {
    this.playlistStateSource.next('loading');
  }

  stopLoading() {
    this.playlistStateSource.next('stopped');
  }

  // To be cleaned up...
  shareLoadedVideo(item: PlaylistItem) {
    const loadedItems = this.itemLoadedSource.getValue();
    loadedItems.push(item);
    loadedItems.sort((a, b) => a.index - b.index);

    this.itemLoadedSource.next(loadedItems);

    if (
      this.itemLoadedSource.getValue().length ===
      this.playlistItemsSource.getValue().length
    ) {
      this.changeState('ready');
    }
  }

  /**
   * Playlist controls
   */

  changeItemPlayed(item: PlaylistItem) {
    this.itemPlayingSource.next(item);
  }

  pauseItem() {
    this.pausedItemSource.next(true);
  }

  updatePlayerProgress(percentage: number) {
    this.playerProgressSource.next(percentage);
  }

  playPreviousItem(item: PlaylistItem) {
    const i = item.index - 1;
    const previousItem = this.itemLoadedSource.getValue()[i];

    if (this.playerProgressSource.getValue() < 2 && !!previousItem) {
      this.changeItemPlayed({ video: previousItem.video, index: i });
    } else {
      this.changeItemPlayed(item);
    }
  }

  playNextItem(item: PlaylistItem) {
    const i = item.index + 1;
    const nextItem = this.itemLoadedSource.getValue()[i];

    if (!!nextItem) {
      this.changeItemPlayed({ video: nextItem.video, index: i });
    } else {
      // Auto replay
      this.changeItemPlayed({
        video: this.itemLoadedSource.getValue()[0].video,
        index: 0
      });
    }
  }
}
