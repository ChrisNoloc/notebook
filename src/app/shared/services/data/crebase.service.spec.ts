import { TestBed, inject } from '@angular/core/testing';

import { CrebaseService } from './crebase.service';

describe('CrebaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrebaseService]
    });
  });

  it('should be created', inject([CrebaseService], (service: CrebaseService) => {
    expect(service).toBeTruthy();
  }));
});
