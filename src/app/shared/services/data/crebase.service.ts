import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { SujetService } from '../subject/sujet.service';
import { VideoService } from '../video/video.service';
import { Theme } from '@shared/models/theme';

@Injectable()
export class CrebaseService {
  constructor(
    private afs: AngularFirestore,
    private ss: SujetService,
    private vs: VideoService
  ) {}

  // Create testing data

  insertAngularVideos(sujet) {
    let videoIds = [
      'mVqQDEtRBwA', // Learn Angular Animations with 5 Examples
      'kl-UMCHpEsw' // Angular Directives - Build a Hold-to-Delete Button
    ];

    videoIds.forEach(id => this.insertGenericVideo(sujet, id));
  }

  insertFirebaseVideos(sujet) {
    let videoIds = [
      'qP5zw7fjQgo', // Firebase Google SignIn + Firestore w/ Angular
      '-GjF9pSeFTs', // Firestore with AngularFire5 Quick Start Tutorial
      '9wxEwE8UFr0' // Angular 6 CRUD Operations With Firebase
    ];

    videoIds.forEach(id => this.insertGenericVideo(sujet, id));
  }

  insertJavascriptVideos(sujet) {
    let videoIds = [
      '2LCo926NFLI', // RxJS Quick Start with Practical Examples
      'ewcoEYS85Co', // RxJS Top Ten - Code This, Not That
      'JWjXBWINlzU' // Custom Rxjs Operators by Example
    ];

    videoIds.forEach(id => this.insertGenericVideo(sujet, id));
  }

  insertQualiteVideos(sujet) {
    let videoIds = [
      'gxixM90vo9Y', // Top 7 Ways to Debug Angular 4 Apps
      'BumgayeUC08', // Angular Testing Quick Start
      '7N63cMKosIE' // Cypress End-to-End Testing
    ];

    videoIds.forEach(id => this.insertGenericVideo(sujet, id));
  }

  insertStateManagementVideos(folder) {
    let videoIds = [
      'f97ICOaekNU', // Angular ngrx Redux Quick Start Tutorial
      '13nWhUndQo4', // Angular Ngrx Effects with Firebase Database
      'wOLF-3wNQh8', // Angular Ngrx with Firebase Google OAuth User Authentication
      '8Wy1AqY5gqE', // Learn @ngrx/entity and Feature Modules
      'rv37jBygQ2g', // NgRx + Firestore
      'SGj11j4hxmg' // NGXS - Angular State Management
    ];

    videoIds.forEach(x => this.insertGenericVideo(folder, x));
  }

  // Utilitary

  private insertGenericVideo(sujet: Theme, youtubeId: string) {
    this.vs.insertVideo(youtubeId, sujet.id);
  }
}
