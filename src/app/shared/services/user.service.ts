import { Injectable } from '@angular/core';

import { combineLatest, Observable } from 'rxjs';

import { switchMap, map, filter, shareReplay } from 'rxjs/operators';

import { AuthService } from './authentication/auth.service';
import { SujetService } from './subject/sujet.service';
import { VideoService } from './video/video.service';
import { FolderTree } from '@shared/models/folder-tree';
import { Theme } from '@shared/models/theme';
import { Video } from '@shared/models/video';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private auth: AuthService,
    private ss: SujetService,
    private vs: VideoService
  ) {}

  user$ = this.auth.user$;

  userFolders$ = combineLatest(this.user$, this.ss.folders$).pipe(
    filter(([user, folders]) => !!user),
    map(([user, folders]) =>
      folders
        .filter(f => f.owner === user.uid)
        .sort((a, b) => (a.libelle < b.libelle ? -1 : 0))
    ),
    shareReplay()
  );

  userFoldersTree$: Observable<FolderTree[]> = combineLatest(this.userFolders$, this.vs.videos$).pipe(
    map(([userFolders, videos]) => {
      const userFoldersTree = [];

      userFolders
        .filter(f => f.themeParent == null)
        .forEach(f =>
          userFoldersTree.push(this.getFolderTree(f, null, videos, userFolders))
        );

      return userFoldersTree;
    })
  );

  // Get Folder Tree of a Single subject
  getFolderTree(
    subject: Theme,
    parentFolder: FolderTree,
    videos: Video[],
    userFolders: Theme[]
  ): FolderTree {
    const newFolder: FolderTree = {
      name: subject.libelle,
      id: subject.id,
      videos: videos.filter(v => v.idSujet === subject.id).length,
      subfolders: [],
      spacing: 0
    };

    if (parentFolder != null) {
      newFolder.spacing = parentFolder.spacing + 1;
    }

    userFolders
      .filter(f => f.themeParent === subject.id)
      .forEach(subfolder =>
        newFolder.subfolders
          .push(this.getFolderTree(subfolder, newFolder, videos, userFolders))
      );

    return newFolder;
  }
}
