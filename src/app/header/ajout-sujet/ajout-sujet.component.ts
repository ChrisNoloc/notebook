import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { filter, switchMap, map } from 'rxjs/operators';
import { VideoService } from '@shared/services/video/video.service';
import { SujetService } from '@shared/services/subject/sujet.service';
import { Theme } from '@shared/models/theme';
import { FolderTree } from '@shared/models/folder-tree';
import { UserService } from '@shared/services/user.service';

@Component({
  selector: 'app-ajout-sujet',
  templateUrl: './ajout-sujet.component.html',
  styleUrls: ['./ajout-sujet.component.scss']
})
export class AjoutSujetComponent implements OnInit {
  @Input() userId: string;

  ajoutSujetForm: FormGroup;
  submitted: boolean;
  showSuccessMessage: boolean;

  userFolders$ = this.us.userFolders$;

  constructor(
    public ss: SujetService,
    private vs: VideoService,
    private us: UserService
  ) {
    this.ajoutSujetForm = new FormGroup({
      $key: new FormControl(null),
      titre: new FormControl('', Validators.required),
      idTheme: new FormControl(null)
    });
  }

  ngOnInit() {
    // Ordered list of the subjects owned by user
    // this.userFolders$ = this.ss.getAllFoldersByOwner(this.userId).pipe(
    //   map(folders =>
    //     folders.sort((a, b) => {
    //       return a.libelle < b.libelle ? -1 : 0;
    //     })
    //   )
    // );
  }

  onSubmit(e) {
    this.submitted = true;

    if (this.ajoutSujetForm.valid) {
      this.submitted = false;

      if (this.ajoutSujetForm.get('$key').value == null) {
        this.ss.insertTheme({
          userId: this.userId,
          ...this.ajoutSujetForm.value
        });
        this.showSuccessMessage = true;
        setTimeout(() => (this.showSuccessMessage = false), 3000);
      }

      this.ajoutSujetForm.reset();
    }
  }
}
