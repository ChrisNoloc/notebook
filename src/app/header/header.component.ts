import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/services/authentication/auth.service';
import { UserService } from '@shared/services/user.service';
import { PlaylistService } from '@shared/services/playlist/playlist.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user$ = this.authService.user$;
  userFoldersTree$ = this.userService.userFoldersTree$;
  playlistState$ = this.playlistService.currentState$;
  playlistItems$ = this.playlistService.playlistItems$;
  brandHovered: boolean;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private playlistService: PlaylistService
  ) {
    this.brandHovered = false;
  }

  ngOnInit() {}

  signOut() {
    this.authService.signOut();
  }
}
