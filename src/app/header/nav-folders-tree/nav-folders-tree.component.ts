import { Component, OnInit, Input } from '@angular/core';
import { FolderTree } from '@shared/models/folder-tree';

@Component({
  selector: 'app-nav-folders-tree',
  templateUrl: './nav-folders-tree.component.html',
  styleUrls: ['./nav-folders-tree.component.scss']
})
export class NavFoldersTreeComponent implements OnInit {

  @Input() folder: FolderTree;

  constructor() { }

  ngOnInit() {
  }

}
