import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavFoldersTreeComponent } from './nav-folders-tree.component';

describe('NavFoldersTreeComponent', () => {
  let component: NavFoldersTreeComponent;
  let fixture: ComponentFixture<NavFoldersTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavFoldersTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavFoldersTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
