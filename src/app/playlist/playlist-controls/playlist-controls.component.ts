import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { PlaylistService } from '@shared/services/playlist/playlist.service';
import { PlaylistItem } from '@shared/models/playlist-item';

@Component({
  selector: 'app-playlist-controls',
  templateUrl: './playlist-controls.component.html',
  styleUrls: ['./playlist-controls.component.scss']
})
export class PlaylistControlsComponent implements OnInit, OnDestroy {
  @Input() firstItem: PlaylistItem;
  @Output() playingPreviousItem = new EventEmitter<PlaylistItem>();
  @Output() playingItem = new EventEmitter<PlaylistItem>();
  @Output() pausingItem = new EventEmitter<boolean>();
  @Output() playingNextItem = new EventEmitter<PlaylistItem>();

  currentState$: Observable<string>;
  itemPlaying: PlaylistItem;
  itemPlayingSubscription: Subscription;
  playerProgressSubscription: Subscription;
  playerProgress$: Observable<number>;

  constructor(private ps: PlaylistService) {}

  ngOnInit() {
    this.currentState$ = this.ps.currentState$;

    // Subscribing to the observable of item playing currently
    this.itemPlayingSubscription = this.ps.itemPlaying$.subscribe(
      item => (this.itemPlaying = item)
    );

    this.playerProgress$ = this.ps.playerProgress$;

    // Giving itemPlaying the value of firstItem if the Observable doesn't contain new value
    if (!!!this.itemPlaying) {
      this.itemPlaying = this.firstItem;
    }
  }

  ngOnDestroy() {
    this.itemPlayingSubscription.unsubscribe();
  }

  playPreviousItem() {
    this.playingPreviousItem.emit(this.itemPlaying);
  }

  playItem() {
    this.playingItem.emit(this.itemPlaying);
  }

  pauseItem() {
    this.pausingItem.emit(true);
  }

  playNextItem() {
    this.playingNextItem.emit(this.itemPlaying);
  }

  // TODO : Loop playlist
}
