import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { PlaylistService } from '@shared/services/playlist/playlist.service';
import { Video } from '@shared/models/video';
import { PlaylistItem } from '@shared/models/playlist-item';
import { tap, filter } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-item',
  templateUrl: './playlist-item.component.html',
  styleUrls: ['./playlist-item.component.scss']
})
export class PlaylistItemComponent implements OnInit, OnDestroy {
  @Input() video: Video;
  @Input() index: number;
  @Output() ended = new EventEmitter<boolean>();
  @Output() progressPlayer = new EventEmitter<any>();
  @Output() readyToLoad = new EventEmitter<boolean>();

  playerVars = {
    cc_lang_pref: 'fr',
    showinfo: 1,
    controls: 1,
    autohide: 1
  };
  player: any;
  progress: number;
  timeUpdateInterval: any;
  currentState = '';
  stateSubscription: Subscription;
  playlistItem: PlaylistItem;
  isReady = false;
  showVideo = false;
  isPlaying = false;
  playingSubscription: Subscription;
  isPaused = false;
  pausedSubscription: Subscription;
  playerProgressInterval: any;

  constructor(private ps: PlaylistService) {}

  ngOnInit() {
    this.playlistItem = {
      video: this.video,
      index: this.index
    };

    // Subscribing to the current state of the playlist
    this.stateSubscription = this.ps.currentState$.subscribe(state => {
      this.currentState = state;

      if (state === 'loading') {
        this.initialize();
      } else if (state === 'stopped') {
        this.stopLoading();
      }
    });

    // Subscribing to the current playing video of the playlist
    this.playingSubscription = this.ps.itemPlaying$.subscribe(item => {
      if (
        this.playlistItem.video === item.video &&
        this.playlistItem.index === item.index
      ) {
        if (this.isPlaying) {
          this.rewind();
        }
        this.playVideo();
      } else {
        this.isPlaying = false;
        this.rewind();
      }
    });

    // Subscribing to the paused state
    this.pausedSubscription = this.ps.pausedItem$
      .pipe(filter(toBePaused => toBePaused && this.isPlaying))
      .subscribe(_ => this.pauseVideo());
  }

  ngOnDestroy() {
    this.stateSubscription.unsubscribe();
    this.playingSubscription.unsubscribe();
    this.pausedSubscription.unsubscribe();
  }

  // When the player is ready
  savePlayer(e) {
    this.player = e;
    this.readyToLoad.emit(true);
  }

  // When the state of the player changes
  onPlayerStateChange(e) {
    // Change the playlist state only if the item is ready to be played
    if (this.showVideo) {
      if (e.data === 1) {
        // Playing : state = 1
      } else if (e.data === 2) {
        // Paused : state = 2
      } else if (e.data === 0) {
        // Ended : state = 0
        this.ended.emit(true);
      }
    }
  }

  initialize() {
    this.isReady = false;

    if (!!this.player) {
      this.player.playVideo();
      this.player.setPlaybackRate(2);
      this.player.mute();

      this.updateLoadingProgress();

      clearInterval(this.timeUpdateInterval);

      this.timeUpdateInterval = setInterval(
        () => this.updateLoadingProgress(),
        1000
      );
    }
  }

  // Updates the progress bar and checks if video is ready to play
  updateLoadingProgress() {
    this.progress =
      (this.player.getCurrentTime() / this.player.getDuration()) * 100;

    if (this.progress === 100) {
      this.getReady();
    }
  }

  stopLoading() {
    if (!!this.player) {
      this.player.pauseVideo();
      clearInterval(this.timeUpdateInterval);
    }
  }

  getReady() {
    this.ps.shareLoadedVideo(this.playlistItem);

    clearInterval(this.timeUpdateInterval);
    this.isReady = true;
    this.rewind();
  }

  // Player controls methods
  playVideo() {
    this.isPlaying = true;
    this.isPaused = false;
    this.showVideo = true;
    this.player.playVideo();
    clearInterval(this.playerProgressInterval);
    this.playerProgressInterval = setInterval(
      () => this.updatePlayerProgress(),
      1000
    );
  }

  pauseVideo() {
    this.isPlaying = false;
    this.isPaused = true;
    this.showVideo = false;
    this.player.pauseVideo();
    clearInterval(this.playerProgressInterval);
  }

  rewind() {
    this.player.seekTo(0);
    this.player.pauseVideo();
    this.player.unMute();
    this.player.setPlaybackRate(1);
    clearInterval(this.playerProgressInterval);
  }

  updatePlayerProgress() {
    const percentage =
      (this.player.getCurrentTime() / this.player.getDuration()) * 100;

    this.progressPlayer.emit(percentage);
  }
}
