import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';
import { PlaylistService } from '@shared/services/playlist/playlist.service';
import { Video } from '@shared/models/video';
import { PlaylistItem } from '@shared/models/playlist-item';
import { Theme } from '@shared/models/theme';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent implements OnInit, OnDestroy {
  currentState$: Observable<string>;
  itemPlaying$: Observable<PlaylistItem>;
  firstItem: PlaylistItem;
  playlistItemsSubscription: Subscription;
  playlistItems: PlaylistItem[];
  readyToLoad = false;

  constructor(private ps: PlaylistService) {}

  ngOnInit() {
    // Observable of the state of the playlist (loading, stopped, ready, playing, paused)
    this.currentState$ = this.ps.currentState$;

    this.playlistItemsSubscription = this.ps.playlistItems$.subscribe(items => {
      this.playlistItems = items;

      // First playlist item to be played when everything is ready : first video in the list of videos
      if (this.playlistItems.length > 0) {
        this.firstItem = {
          video: this.playlistItems[0].video,
          index: 0
        };

        // Wait for 4 seconds and let the user load the playlist
        setTimeout(_ => (this.readyToLoad = true), 4000);
      }
    });

    // Observable of the playlist item currently playing
    this.itemPlaying$ = this.ps.itemPlaying$;
  }

  ngOnDestroy() {
    this.resetPlayer();
  }

  loadPlaylist() {
    this.ps.changeState('loading');
  }

  stopLoading() {
    this.ps.changeState('readyToLoad');
  }

  updateProgressPlayer(percentage: number) {
    this.ps.updatePlayerProgress(percentage);
  }

  notifyItemReadyToLoad(item: PlaylistItem) {
    this.ps.itemReadyToLoad(item);
  }

  resetPlayer() {
    this.ps.updatePlayerProgress(0);
    this.ps.changeState('stopped');
    this.firstItem = null;
  }

  /**
   * Actions when Player controls emit an event
   */

  playPreviousItem(item: PlaylistItem) {
    this.ps.playPreviousItem(item);
  }

  playItem(item: PlaylistItem) {
    this.ps.changeItemPlayed(item);
    this.ps.changeState('playing');
  }

  pauseItem(item) {
    this.ps.pauseItem();
    this.ps.changeState('paused');
  }

  playNextItem(item: PlaylistItem) {
    this.ps.playNextItem(item);
  }
}
