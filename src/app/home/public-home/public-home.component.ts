import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/services/authentication/auth.service';

@Component({
  selector: 'app-public-home',
  templateUrl: './public-home.component.html',
  styleUrls: ['./public-home.component.scss']
})
export class PublicHomeComponent implements OnInit {
  constructor(private auth: AuthService) {}

  ngOnInit() {}

  googleSignIn() {
    this.auth.googleSignIn();
  }
}
