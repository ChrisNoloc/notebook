// Angular
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';

// Modules
import { AppRoutingModule } from './app-routing.module';

// Firabase : Cloud Firestore
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

// Ngx Youtube Player Module
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';

// Services
import { VideoService } from '@shared/services/video/video.service';
import { SujetService } from '@shared/services/subject/sujet.service';
import { CrebaseService } from '@shared/services/data/crebase.service';

// Components
import { AppComponent } from './app.component';
import { AjoutSujetComponent } from './header/ajout-sujet/ajout-sujet.component';
import { LoadingComponent } from '@shared/loading/loading.component';
import { VideoComponent } from './subject/video/video.component';
import { SubjectComponent } from './subject/subject.component';
import { NavFoldersTreeComponent } from './header/nav-folders-tree/nav-folders-tree.component';
import { PublicHomeComponent } from './home/public-home/public-home.component';
import { TrackComponent } from './subject/track/track.component';
import { PlaylistItemComponent } from './playlist/playlist-item/playlist-item.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { PlaylistControlsComponent } from './playlist/playlist-controls/playlist-controls.component';
import { HeaderComponent } from './header/header.component';

// Pipes
import { SpacingPipe } from '@shared/pipes/spacing.pipe';
import { MomentPipe } from '@shared/pipes/moment.pipe';
import { YoutubePipe } from '@shared/pipes/youtube.pipe';
import { MinutesSecondsPipe } from '@shared/pipes/minutes-seconds.pipe';

// Directives
import { HoldableDirective } from '@shared/directives/holdable.directive';
import { EditFolderModalComponent } from './subject/edit-folder-modal/edit-folder-modal.component';
import { AddVideoModalComponent } from './subject/add-video-modal/add-video-modal.component';
import { AddSubfolderModalComponent } from './subject/add-subfolder-modal/add-subfolder-modal.component';
import { SubfolderComponent } from './subject/subfolder/subfolder.component';


@NgModule({
  declarations: [
    AppComponent,
    AjoutSujetComponent,
    YoutubePipe,
    VideoComponent,
    SubjectComponent,
    LoadingComponent,
    SpacingPipe,
    MomentPipe,
    HoldableDirective,
    NavFoldersTreeComponent,
    PublicHomeComponent,
    TrackComponent,
    PlaylistItemComponent,
    PlaylistComponent,
    MinutesSecondsPipe,
    PlaylistControlsComponent,
    HeaderComponent,
    EditFolderModalComponent,
    AddVideoModalComponent,
    AddSubfolderModalComponent,
    SubfolderComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    NgxYoutubePlayerModule.forRoot()
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'fr-FR'
    },
    VideoService,
    SujetService,
    CrebaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
registerLocaleData(localeFr, 'fr-FR');
