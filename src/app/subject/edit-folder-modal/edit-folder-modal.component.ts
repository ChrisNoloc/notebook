import { Component, OnInit, Input } from '@angular/core';
import { Theme } from '@shared/models/theme';
import { CrebaseService } from '@shared/services/data/crebase.service';
import { SujetService } from '@shared/services/subject/sujet.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-folder-modal',
  templateUrl: './edit-folder-modal.component.html',
  styleUrls: ['./edit-folder-modal.component.scss']
})
export class EditFolderModalComponent implements OnInit {
  @Input() subject: Theme;

  triedDelete: boolean;
  deletionProgress: number;

  constructor(
    private crebaseService: CrebaseService,
    private sujetService: SujetService,
    private router: Router
  ) {
    this.triedDelete = false;
  }

  ngOnInit() {}

  editSubject() {
    this.sujetService.updateTheme(this.subject);
  }

  tryDelete() {
    this.triedDelete = true;
    setTimeout(() => (this.triedDelete = false), 2000);
  }

  deleteSubject(e) {
    this.deletionProgress = e / 10;

    if (this.deletionProgress === 100) {
      this.sujetService.deleteTheme(this.subject);
      document.getElementById('close' + this.subject.id).click();
      this.router.navigate([
        !!this.subject.themeParent
          ? `/subject/${this.subject.themeParent}`
          : '/'
      ]);
    }
  }

  /**
   * Crebase functions
   */

  insertAngularVideos(subject: Theme) {
    this.crebaseService.insertAngularVideos(subject);
  }

  insertFirebaseVideos(subject: Theme) {
    this.crebaseService.insertFirebaseVideos(subject);
  }

  insertJavascriptVideos(subject: Theme) {
    this.crebaseService.insertJavascriptVideos(subject);
  }

  insertQualiteVideos(subject: Theme) {
    this.crebaseService.insertQualiteVideos(subject);
  }

  insertStateManagementVideos(subject: Theme) {
    this.crebaseService.insertStateManagementVideos(subject);
  }
}
