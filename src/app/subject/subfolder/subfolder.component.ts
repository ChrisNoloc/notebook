import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Theme } from '@shared/models/theme';
import { VideoService } from '@shared/services/video/video.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subfolder',
  templateUrl: './subfolder.component.html',
  styleUrls: ['./subfolder.component.scss']
})
export class SubfolderComponent implements OnInit, OnDestroy {
  @Input() subfolder: Theme;
  videosThumbnails: string[];
  videos: number;
  videosSubscription: Subscription;

  constructor(private videoService: VideoService) {
    this.videosThumbnails = [];
    this.videos = 0;
  }

  ngOnInit() {
    this.videosSubscription = this.videoService
      .getAllVideosByIdSujet(this.subfolder.id)
      .subscribe(videos => {
        this.videos = videos.length;

        videos.forEach(video => {
          if (this.videosThumbnails.length < 4) {
            this.videosThumbnails.push(video.thumbnails.medium.url);
          }
        });
      });
  }

  ngOnDestroy() {
    this.videosSubscription.unsubscribe();
  }
}
