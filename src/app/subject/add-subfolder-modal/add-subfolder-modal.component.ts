import { Component, OnInit, Input } from '@angular/core';
import { Theme } from '@shared/models/theme';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SujetService } from '@shared/services/subject/sujet.service';

@Component({
  selector: 'app-add-subfolder-modal',
  templateUrl: './add-subfolder-modal.component.html',
  styleUrls: ['./add-subfolder-modal.component.scss']
})
export class AddSubfolderModalComponent implements OnInit {
  @Input() subject: Theme;
  @Input() user: any;
  subjectFormGroup: FormGroup;
  submittedSubject: boolean;

  constructor(private sujetService: SujetService) {
    this.submittedSubject = false;

    // Init subject form group
    this.subjectFormGroup = new FormGroup({
      $key: new FormControl(null),
      titre: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {}

  submitSubject() {
    this.submittedSubject = true;

    if (this.subjectFormGroup.valid) {
      this.submittedSubject = false;

      if (this.subjectFormGroup.get('$key').value == null) {
        const newSubject = {
          userId: this.user.uid,
          idTheme: this.subject.id,
          ...this.subjectFormGroup.value
        };

        this.sujetService.insertTheme(newSubject);
        console.log(this.submittedSubject);
        this.submittedSubject = true;
        setTimeout(() => (this.submittedSubject = false), 3000);
      }

      this.subjectFormGroup.reset();
    }
  }
}
