import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubfolderModalComponent } from './add-subfolder-modal.component';

describe('AddSubfolderModalComponent', () => {
  let component: AddSubfolderModalComponent;
  let fixture: ComponentFixture<AddSubfolderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubfolderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubfolderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
