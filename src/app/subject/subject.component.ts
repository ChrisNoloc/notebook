import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { SujetService } from '@shared/services/subject/sujet.service';
import { VideoService } from '@shared/services/video/video.service';
import { CrebaseService } from '@shared/services/data/crebase.service';
import { AuthService } from '@shared/services/authentication/auth.service';
import { Theme } from '@shared/models/theme';
import { Video } from '@shared/models/video';
import { PlaylistService } from '@shared/services/playlist/playlist.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {
  subjectFormGroup: FormGroup;
  submittedSubject: boolean;

  user$: Observable<any>;

  folderId: string;
  folder$: Observable<Theme>;
  videos$: Observable<Video[]>;
  subfolders$: Observable<any[]>;
  parentFolder$: Observable<Theme>;

  constructor(
    private sujetService: SujetService,
    private videoService: VideoService,
    public crebaseService: CrebaseService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private playlistService: PlaylistService
  ) {}

  ngOnInit() {
    this.user$ = this.authService.user$;

    // From activated route, get the Folder's ID and the Observables
    this.folder$ = this.route.paramMap.pipe(
      switchMap(params =>
        this.sujetService.getThemeById(params.get('idSubject'))
      )
    );

    this.videos$ = this.route.paramMap.pipe(
      switchMap(params =>
        this.videoService.getAllVideosByIdSujet(params.get('idSubject'))
      )
    );

    this.parentFolder$ = this.folder$.pipe(
      switchMap(folder => this.sujetService.getThemeById(folder.themeParent))
    );

    this.subfolders$ = this.route.paramMap.pipe(
      switchMap(params =>
        this.sujetService.getAllSujetsByIdTheme(params.get('idSubject'))
      )
    );

    // Get subfolders with its video thumbnails
    // this.subfolders$ = this.route.paramMap.pipe(
    //   switchMap(params =>
    //     this.sujetService.getAllSujetsByIdTheme(params.get('idSubject')).pipe(
    //       switchMap(folders => {
    //         const subFolders = [];

    //         folders.forEach(f => {
    //           const subfolder = {
    //             subject: f,
    //             videos: 0,
    //             videosThumbnails: [],
    //             themeParent: this.folderId
    //           };

    //           this.videoService
    //             .getAllVideosByIdSujet(f.id)
    //             .subscribe(videos => {
    //               subfolder.videos = videos.length;

    //               videos.forEach(video => {
    //                 if (subfolder.videosThumbnails.length < 4) {
    //                   subfolder.videosThumbnails.push(
    //                     video.thumbnails.medium.url
    //                   );
    //                 }
    //               });

    //               // Verify if subfolder isn't in the array and the folder's subject corresponds to the subject
    //               const isDuplicate: Boolean = subFolders.some(
    //                 s => s.subject.id == subfolder.subject.id
    //               );
    //               if (!isDuplicate) {
    //                 subFolders.push(subfolder);
    //               }
    //             });
    //         });

    //         return of(subFolders);
    //       })
    //     )
    //   )
    // );
  }

  // Create a playlist based on the subject
  launchPlaylist(videos: Video[]) {
    const playlistItems = [];

    for (let i = 0; i < videos.length; i++) {
      playlistItems.push({
        index: i,
        video: videos[i]
      });
    }

    this.playlistService.setPlaylistItems(playlistItems);

    this.router.navigate(['/playlist']);
  }
}
