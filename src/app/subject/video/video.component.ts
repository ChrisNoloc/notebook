declare var $;
import * as $ from 'jquery';

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { VideoService } from '@shared/services/video/video.service';
import { Video } from '@shared/models/video';
import { PlaylistService } from '@shared/services/playlist/playlist.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  @Input() video: Video;

  progress: number;
  triedDelete = false;
  playerVars = {
    cc_lang_pref: 'fr'
  };
  private player;
  private ytEvent;

  showPlayer: boolean = false;

  constructor(
    private vs: VideoService,
    private router: Router,
    private playlistService: PlaylistService
  ) {}

  ngOnInit() {
    // Enable Bootstrap tooltip
    $(function() {
      $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
    });
  }

  deleteVideo(e) {
    this.progress = e / 10;

    if (this.progress === 100) {
      this.vs.deleteVideo(this.video);
      document.getElementById('close' + this.video.id).click();
    }
  }

  tryDelete() {
    this.triedDelete = true;
    setTimeout(() => (this.triedDelete = false), 2000);
  }

  // ngx youtube player

  onStateChange(event) {
    this.ytEvent = event.data;
    console.log('player state : ', this.ytEvent);
  }

  savePlayer(player) {
    this.player = player;
    console.log('player instance : ', player);
  }

  playVideo() {
    this.player.playVideo();
  }

  pauseVideo() {
    this.player.pauseVideo();
  }

  togglePlayer() {
    this.showPlayer = !this.showPlayer;
  }

  addToPlaylist() {
    this.playlistService.addVideoToPlaylist(this.video);
  }
}
