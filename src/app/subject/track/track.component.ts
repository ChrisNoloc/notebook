import { Component, OnInit, Input } from '@angular/core';
import reframe from 'reframe.js';
import { Video } from '@shared/models/video';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {
  @Input() video: Video;

  YT: any;
  player: any;
  reframed: Boolean = false;

  constructor() {}

  init() {
    // Put a script tag to load the YouTube iframe api in the HTML page
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';

    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  ngOnInit() {
    this.init();

    // When the player is ready
    window['onYoutubeIframeAPIReady'] = e => {
      this.YT = window['YT'];
      this.reframed = false;
      this.player = new window['YT'].Player('player', {
        videoId: this.video.youtubeId,
        events: {
          onStateChange: this.onPlayerStateChange.bind(this),
          onError: this.onPlayerError.bind(this),
          onReady: e => {
            if (!this.reframed) {
              this.reframed = true;
              reframe(e.target.a);
            }
          }
        }
      });
    };
  }

  onPlayerStateChange(event) {
    console.log(event);

    switch (event.data) {
      case window['YT'].PlayerState.PLAYING:
        if (this.cleanTime() == 0) {
          console.log('started ' + this.cleanTime());
        } else {
          console.log('playing ' + this.cleanTime());
        }

        break;

      case window['YT'].PlayerState.PAUSED:
        if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
          console.log('paused @ ' + this.cleanTime());
        }

        break;

      case window['YT'].PlayerState.ENDED:
        console.log('ended ');

        break;
    }
  }

  onPlayerError(event) {
    switch (event.data) {
      case 2:
        console.log('' + this.video.youtubeId);
        break;

      case 100:
        break;

      case 101 || 150:
        break;
    }
  }

  cleanTime() {
    return Math.round(this.player.getCurrentTime());
  }
}
