import { Component, OnInit, Input } from '@angular/core';
import { Theme } from '@shared/models/theme';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { VideoService } from '@shared/services/video/video.service';

@Component({
  selector: 'app-add-video-modal',
  templateUrl: './add-video-modal.component.html',
  styleUrls: ['./add-video-modal.component.scss']
})
export class AddVideoModalComponent implements OnInit {
  @Input() subject: Theme;
  videoFormGroup: FormGroup;
  successVideoSubmit: boolean;

  constructor(private videoService: VideoService) {
    /*
      TODO : custom Validators for Youtube Link : https://www.youtube.com/watch?v=XXXXXXXXX
    */
    // Init video form group
    this.videoFormGroup = new FormGroup({
      $key: new FormControl(null),
      input: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {}

  submitVideo() {
    this.successVideoSubmit = true;

    if (this.videoFormGroup.valid) {
      this.successVideoSubmit = false;

      if (this.videoFormGroup.get('$key').value == null) {
        const link = {
          idTheme: this.subject.themeParent,
          idSujet: this.subject.id,
          ...this.videoFormGroup.value
        };

        this.videoService.processSubmittedLink(link);
        this.successVideoSubmit = true;
        setTimeout(() => (this.successVideoSubmit = false), 3000);
      }

      this.videoFormGroup.reset();
    }
  }
}
