// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBF7fj4KiGHMQOiHIX_xWCnjXXdPWpdiOU',
    authDomain: 'notebook-6ba35.firebaseapp.com',
    databaseURL: 'https://notebook-6ba35.firebaseio.com',
    projectId: 'notebook-6ba35',
    storageBucket: 'notebook-6ba35.appspot.com',
    messagingSenderId: '973598045454'
  }
};
